import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Mfe1Module } from './mfe1/mfe1.module';
import { MfeCardModule } from './mfe1/mfe-card/mfe-card.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Mfe1Module,
    MfeCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
