import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MfeCardComponent } from './mfe1/mfe-card/mfe-card.component';
import { MfeCardModule } from './mfe1/mfe-card/mfe-card.module';

const routes: Routes = [
  {
    path: '',
    component: MfeCardComponent
    //loadChildren: () => import('./mfe1/mfe-card/mfe-card.module').then(m => m.MfeCardModule)
    
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
