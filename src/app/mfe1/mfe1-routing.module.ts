import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MfeCardComponent } from './mfe-card/mfe-card.component';
import { MfeDetailComponent } from './mfe-detail/mfe-detail.component';
import { Mfe1Component } from './mfe1.component';

const routes: Routes = [
  {
    path: '',
    component: Mfe1Component
  },
  {
    path: 'detail',
    component: MfeDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Mfe1RoutingModule { }
