import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mfe1',
  templateUrl: './mfe1.component.html',
  styleUrls: ['./mfe1.component.css']
})
export class Mfe1Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
    console.log("MFE! componente");
  }

}
