import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Mfe1Component } from './mfe1.component';
import { Mfe1RoutingModule } from './mfe1-routing.module';
import { MfeDetailComponent } from './mfe-detail/mfe-detail.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button'
import {MatCardModule} from '@angular/material/card';
@NgModule({
  declarations: [
    Mfe1Component,
    MfeDetailComponent,
  ],
  imports: [
    CommonModule,
    Mfe1RoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule
  ]
})
export class Mfe1Module { }
