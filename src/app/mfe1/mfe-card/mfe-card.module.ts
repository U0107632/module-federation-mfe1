import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MfeCardComponent } from './mfe-card.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Mfe1CardRoutingModule } from './mfe-card-routing.module';



@NgModule({
  declarations: [
    MfeCardComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    Mfe1CardRoutingModule
  ],
  exports: [MfeCardComponent]
})
export class MfeCardModule { }
