import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  
  {
    path: 'mfe1',
    //component: Mfe1Component
    loadChildren: () => import('../mfe1.module').then(m => m.Mfe1Module)

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Mfe1CardRoutingModule { }
