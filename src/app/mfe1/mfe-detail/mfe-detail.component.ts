import { Component, OnInit } from '@angular/core';

import {Location} from '@angular/common';

@Component({
  selector: 'app-mfe-detail',
  templateUrl: './mfe-detail.component.html',
  styleUrls: ['./mfe-detail.component.css']
})
export class MfeDetailComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit(): void {
  }

  goBack(){
    this.location.back();
  }
}
