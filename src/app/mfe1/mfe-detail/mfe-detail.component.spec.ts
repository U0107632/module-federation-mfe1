import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MfeDetailComponent } from './mfe-detail.component';

describe('MfeDetailComponent', () => {
  let component: MfeDetailComponent;
  let fixture: ComponentFixture<MfeDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MfeDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MfeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
